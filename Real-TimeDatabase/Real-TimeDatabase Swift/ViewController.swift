//
//  ViewController.swift
//  Real-TimeDatabase Swift
//
//  Created by WASIQ-MACBOOK on 15/12/2020.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit


class ViewController: UIViewController {
    var email = ""
    var useruid = "g4jJm59S7OdQ9YTf6ECskKpUyGF2"
    var name = "Wasiq"
    var ref: DatabaseReference!
    let userId = Auth.auth().currentUser?.uid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func writeButtomnPressed(_ sender: UIButton) {
        
        self.ref.child("posts").child(useruid).setValue(["name": name])

        print(self.name)
        
        
    }
    
    @IBAction func facebookButtonPressed(_ sender: UIButton) {
        
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = AccessToken.current else {
                print("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            // Perform login by calling Firebase APIs
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
               
                self.getFBUserData()
            })
            
        }
        
        
    }
    
    func getFBUserData(){
        
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,age_range"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    //print(dict)
                    if let dict = result as? [String : AnyObject]{
                        if(dict["email"] as? String == nil || dict["id"] as? String == nil || dict["email"] as? String == "" || dict["id"] as? String == "" ){
                            
                        }else{
                            
                            self.email = (dict["email"] as? String)!
                            print(self.email)
                            
                            
                           
                            
                        }
                    }
                    
                }
            })
        }
        
    }
    
    
    
    
    @IBAction func readButtonPressed(_ sender: UIButton) {
        let a = Auth.auth().currentUser?.uid
        print("aa: ",a)

        ref.child("posts").child("\(useruid)").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let name = value?["name"] as? String ?? ""
            print(name)
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
}

